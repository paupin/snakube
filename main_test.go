package main

import (
	"reflect"
	"testing"
)

func TestSnakeLength(t *testing.T) {
	length := 0
	for _, l := range snake {
		length += l
	}
	if length != CubeSize {
		t.Errorf("expected: %d but got instead: %d\n", CubeSize, length)
	}
}

func TestLinesWrite(t *testing.T) {
	var actual Lines
	check := func(r, c int, s string, wanted ...string) {
		t.Helper()
		actual.Write(r, c, s)
		expected := Lines(wanted)
		if !reflect.DeepEqual(actual, expected) {
			t.Errorf("bad!\nexpected:    %+v\ngot instead: %+v\n", expected, actual)
		}
	}

	check(0, 0, "hey", "hey")
	check(0, 4, "hey", "hey hey")
	check(3, 1, "WAT", "hey hey", "", "", " WAT")
	check(0, 3, "WOW", "heyWOWy", "", "", " WAT")
	check(0, 7, "Loll", "heyWOWyLoll", "", "", " WAT")
	check(1, 10, "!", "heyWOWyLoll", "          !", "", " WAT")
}

func TestDirectionNext(t *testing.T) {
	check := func(d Direction, expected ...Direction) {
		t.Helper()
		actual := d.Next()
		ok := len(expected) == len(actual)
		for i, a := range actual {
			if ok && expected[i] != a {
				ok = false
				break
			}
		}
		if !ok {
			t.Errorf("bad!\nexpected:    %+v\ngot instead: %+v\n", expected, actual)
		}
	}

	check(right, down, up, back, front)
	check(left, down, up, back, front)
	check(up, left, right, back, front)
	check(down, left, right, back, front)
	check(front, left, right, down, up)
	check(back, left, right, down, up)
}
