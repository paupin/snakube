# Snakube

This program brute forces a solution to a traditional wooden puzzle,
formed by 27 small wooden cubes, connected by strings,
that when solved must form a larger 3x3 cube.

![Solved cube](solved.jpg)

On some of the cubes the string comes out on the opposite side, and on others it
comes out on an adjacent side. We call this series of cubes "the snake",
and this is its rough draft, as if it was laid flat on a table:

![Snake](snake.jpg)

```
                R S T
A   G H I     P Q   U V W
B   F   J K   O         X Y
C D E     L M N           Z
                          Å
```

Though the snake rotates at each square, in practice we only need to take into
consideration rotations before the snake changes direction. For example, the
rotation between "A" and "B" irrelevant, but the rotation between "B" and "C"
does matter. So we consider A-B a segment, C-D another, etc.

The list of segments, by length, is: 2,2,2,2,1,1,1,2,2,1,1,2,1,2,1,1,3.

## Running/testing

With Golang installed, just do `go test ./...` or `go run .`.

## Q&A

Q: Does it work?

A: Of course it does!

Q: Wouldn't it be be faster to try all combinations by hand?

A: [Probably](https://xkcd.com/974/)...

Q: What is the actual solution?

A: You go: right, up, left, front, right, back, right, down, left, front, up, back, right, front, down, right, up.
