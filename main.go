package main

import (
	"fmt"
)

type Direction [3]int

var (
	right = Direction{1, 0, 0}
	left  = Direction{-1, 0, 0}
	up    = Direction{0, 1, 0}
	down  = Direction{0, -1, 0}
	front = Direction{0, 0, 1}
	back  = Direction{0, 0, -1}
)

func (d Direction) String() string {
	name := map[Direction]string{right: "R", left: "L", up: "U", down: "D", front: "F", back: "B"}
	return name[d]
}

// Next returns all possible directions coming out of this one
func (d Direction) Next() []Direction {
	switch d {
	case right, left:
		return []Direction{down, up, back, front}
	case up, down:
		return []Direction{left, right, back, front}
	default: // front, back
		return []Direction{left, right, down, up}
	}
}

type Position [3]int

func (p Position) String() string {
	x, y, z := p[0], p[1], p[2]
	return fmt.Sprintf("(%d,%d,%d)", x, y, z)
}

func (p Position) Valid() bool {
	x, y, z := p[0], p[1], p[2]
	return x >= 0 && x < 3 &&
		y >= 0 && y < 3 &&
		z >= 0 && z < 3
}

func (p Position) Offset(a Direction) Position {
	for i, delta := range a {
		p[i] += delta
	}
	return p
}

type Snake []int

type Cube [3][3][3]bool

func (c Cube) Count() (filled int) {
	for x := 0; x < 3; x++ {
		for y := 0; y < 3; y++ {
			for z := 0; z < 3; z++ {
				if c[x][y][z] {
					filled++
				}
			}
		}
	}
	return
}

const CubeSize = 3 * 3 * 3

func (c Cube) Solved() bool {
	return c.Count() == CubeSize
}

func (c Cube) Print(l *Lines) {
	for y := 0; y < 3; y++ {
		startcol := 5 + y*11
		for z := 0; z < 3; z++ {
			if z == 0 {
				l.Write(0, startcol-4, "y=%d", y)
			}
			for x := 0; x < 3; x++ {
				s := "."
				if c[x][y][z] {
					s = "X"
				}
				l.Write(z, startcol+x*2, s)
			}
		}
	}
}

func solve(snake Snake, dirs []Direction, idx int, p Position, c Cube) bool {
	// start by filling the segment on the current direction
	length := snake[idx]
	dir := dirs[idx]
	for l := 0; l < length; l++ {
		if !p.Valid() {
			// don't think outside the box
			return false
		}
		x, y, z := p[0], p[1], p[2]
		if c[x][y][z] {
			// position already filled
			return false
		}
		c[x][y][z] = true
		p = p.Offset(dir)
	}

	// write some debug info
	var l Lines
	c.Print(&l)
	l.Write(0, 35, "index %d of %d, len %d dir %s", idx, len(snake), length, dir)
	l.Write(1, 35, "directions: %v", dirs)
	fmt.Println(l.String())

	// are we there yet?
	nextindex := idx + 1
	if nextindex >= len(snake) {
		return c.Solved()
	}

	// iterate all possible directions from here
	for _, d := range dir.Next() {
		dirs[nextindex] = d
		// snake and dirs are slices, so they will be passed by reference;
		// p and c are arrays, so they will be passed by value
		if solve(snake, dirs, nextindex, p, c) {
			return true
		}
	}
	return false
}

var snake = Snake{2, 2, 2, 2, 1, 1, 1, 2, 2, 1, 1, 2, 1, 2, 1, 1, 3}

func main() {
	// start at (0,0,0) going right on an empty cube
	var p Position
	dirs := make([]Direction, len(snake))
	dirs[0] = right
	var c Cube

	if !solve(snake, dirs, 0, p, c) {
		panic("no solution")
	}

	name := map[Direction]string{right: "right", left: "left", up: "up", down: "down", front: "front", back: "back"}

	fmt.Printf("Solved! Directions: ")
	for i, d := range dirs {
		if i > 0 {
			fmt.Printf(", ")
		}
		fmt.Print(name[d])
	}
	fmt.Printf(".\n")
}
