package main

import (
	"fmt"
	"strings"
)

// Lines is a helper class, used to format a set of lines.
// Think of it a poor man's ncurses.
type Lines []string

func (ls *Lines) Write(row, col int, format string, a ...interface{}) {
	// ensure we have enough rows
	for row > len(*ls)-1 {
		*ls = append(*ls, "")
	}

	// ensure the row is big enough to contain the message
	line := (*ls)[row]
	s := fmt.Sprintf(format, a...)
	endcol := col + len(s)
	if missing := (endcol) - len(line); missing > 0 {
		line += strings.Repeat(" ", missing)
	}

	// replace row
	(*ls)[row] = line[:col] + s + line[endcol:]
}

func (ls *Lines) String() string {
	return strings.Join(*ls, "\n")
}
